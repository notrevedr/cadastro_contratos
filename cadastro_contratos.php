<?php

$hostname = "localhost";
$username = "root";
$password = "admin123";
$database = "test";

$conn = mysqli_connect($hostname, $username, $password, $database);

if (!$conn) die("Conexão falhou: " . mysqli_connect_error());

$sqlQuery = "SELECT b.nome AS nome_banco, co.verba, c.codigo AS codigo_contrato, c.data_inclusao, c.valor, c.prazo FROM Tb_banco b INNER JOIN Tb_convenio co ON b.codigo = co.banco INNER JOIN Tb_contrato c ON co.codigo = c.convenio_servico";

$result = mysqli_query($conn, $sqlQuery);

if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        echo "Banco: " . $row["nome_banco"] . " - " . "Verba: " . $row["verba"] . " - " . "Código do contrato: " . $row["codigo_contrato"] . " - " . "Data de inclusão: " . $row["data_inclusao"] . " - " . "Valor: " . $row["valor"] . " - " . "Prazo: " . $row["prazo"] . "\n"; 
    }
} else {
    echo "Nenhum resultado encontrado!";
}

mysqli_close($conn);